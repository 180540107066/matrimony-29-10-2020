package com.example.matrimony_29_10_2020.model;

import java.io.Serializable;

public class LanguageModel implements Serializable {

    int LanguageId;
    String Name;

    public int getLanguageId() {
        return LanguageId;
    }

    public void setLanguageId(int languageId) {
        LanguageId = languageId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "LanguageModel{" +
                "LanguageId=" + LanguageId +
                ", Name='" + Name + '\'' +
                '}';
    }
}
