package com.example.matrimony_29_10_2020.model;

import java.io.Serializable;

public class CityModel implements Serializable {

    int CityId;
    String Name;

    public int getCityId() {
        return CityId;
    }

    public void setCityId(int cityId) {
        CityId = cityId;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    @Override
    public String toString() {
        return "CityModel{" +
                "CityId=" + CityId +
                ", Name='" + Name + '\'' +
                '}';
    }
}
