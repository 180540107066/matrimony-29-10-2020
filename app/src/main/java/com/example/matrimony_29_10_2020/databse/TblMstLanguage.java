package com.example.matrimony_29_10_2020.databse;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony_29_10_2020.model.LanguageModel;

import java.util.ArrayList;

public class TblMstLanguage extends MyDatabase {

    public static final String TABLE_NAME = "TblMstLanguage";
    public static final String LANGUAGE_ID = "LanguageId";
    public static final String NAME = "Name";


    public TblMstLanguage(Context context) {
        super(context);
    }

    public ArrayList<LanguageModel> getCityList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<LanguageModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            LanguageModel languageModel = new LanguageModel();
            languageModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            languageModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            list.add(languageModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public LanguageModel getLanguageId(int id) {
        SQLiteDatabase db = getReadableDatabase();
        LanguageModel languageModel = new LanguageModel();
        String query = " SELECT * FROM " + TABLE_NAME + " Where " + LANGUAGE_ID + " =?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        languageModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        languageModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
        cursor.close();
        db.close();
        return languageModel;
    }
}
