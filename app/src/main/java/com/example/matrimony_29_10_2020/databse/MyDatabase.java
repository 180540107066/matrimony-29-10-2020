package com.example.matrimony_29_10_2020.databse;

import android.content.Context;

import com.readystatesoftware.sqliteasset.SQLiteAssetHelper;


public class MyDatabase extends SQLiteAssetHelper {

    public static final String DATABASE_NAME = "Matrimany.db";
    public static final int DATABASE_VERSION = 1;

    public MyDatabase(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }
}
