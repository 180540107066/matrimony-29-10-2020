package com.example.matrimony_29_10_2020.databse;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import com.example.matrimony_29_10_2020.model.UserModel;

import java.util.ArrayList;

public class TblUser extends MyDatabase {

    public static final String TABLE_NAME = "TblUser";
    public static final String USER_ID = "UserId";
    public static final String NAME = "Name";
    public static final String FATHER_NAME = "FatherName";
    public static final String SUR_NAME = "SurName";
    public static final String EMAIL = "Email";
    public static final String DOB = "Dob";
    public static final String CITY_ID = "CityId";
    public static final String LANGUAGE_ID = "LanguageId";
    public static final String PHONE_NUMBER = "PhoneNumber";


    public TblUser(Context context) {
        super(context);
    }

    public ArrayList<UserModel> getUserList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            UserModel userModel = new UserModel();
            userModel.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
            userModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            userModel.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
            userModel.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
            userModel.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            userModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
            userModel.setDob(cursor.getString(cursor.getColumnIndex(DOB)));
            userModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            userModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            list.add(userModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }

    public UserModel getUserId(int id) {
        SQLiteDatabase db = getReadableDatabase();
        UserModel userModel = new UserModel();
        String query = " SELECT * FROM " + TABLE_NAME + " Where " + USER_ID + " =?";
        Cursor cursor = db.rawQuery(query, new String[]{String.valueOf(id)});
        cursor.moveToFirst();
        userModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
        userModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
        cursor.close();
        db.close();
        return userModel;
    }

    public ArrayList<UserModel> getGenderList() {
        SQLiteDatabase db = getReadableDatabase();
        ArrayList<UserModel> list = new ArrayList<>();
        String query = "SELECT * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(query, null);
        cursor.moveToFirst();
        for (int i = 0; i < cursor.getCount(); i++) {
            UserModel userModel = new UserModel();
            userModel.setUserId(cursor.getInt(cursor.getColumnIndex(USER_ID)));
            userModel.setName(cursor.getString(cursor.getColumnIndex(NAME)));
            userModel.setFatherName(cursor.getString(cursor.getColumnIndex(FATHER_NAME)));
            userModel.setSurName(cursor.getString(cursor.getColumnIndex(SUR_NAME)));
            userModel.setEmail(cursor.getString(cursor.getColumnIndex(EMAIL)));
            userModel.setPhoneNumber(cursor.getString(cursor.getColumnIndex(PHONE_NUMBER)));
            userModel.setDob(cursor.getString(cursor.getColumnIndex(DOB)));
            userModel.setCityId(cursor.getInt(cursor.getColumnIndex(CITY_ID)));
            userModel.setLanguageId(cursor.getInt(cursor.getColumnIndex(LANGUAGE_ID)));
            list.add(userModel);
            cursor.moveToNext();
        }
        cursor.close();
        db.close();
        return list;
    }
}
