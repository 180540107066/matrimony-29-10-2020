package com.example.matrimony_29_10_2020.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.matrimony_29_10_2020.model.CityModel;

import java.util.ArrayList;

public class CityAdapter extends BaseAdapter {
    Context context;
    ArrayList<CityModel> cityList;

    public CityAdapter(Context context, ArrayList<CityModel> cityList) {
        this.context = context;
        this.cityList = cityList;
    }

    @Override
    public int getCount() {
        return cityList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {

//        v= LayoutInflater.from(context).inflate();
        return null;
    }
}
