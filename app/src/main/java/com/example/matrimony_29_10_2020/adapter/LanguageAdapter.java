package com.example.matrimony_29_10_2020.adapter;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.example.matrimony_29_10_2020.model.LanguageModel;

import java.util.ArrayList;

public class LanguageAdapter extends BaseAdapter {
    Context context;
    ArrayList<LanguageModel> languageList;

    public LanguageAdapter(Context context, ArrayList<LanguageModel> languageList) {
        this.context = context;
        this.languageList = languageList;
    }

    @Override
    public int getCount() {
        return languageList.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {


        return null;
    }
}
